#!/usr/bin/env python2

# Copyright (C) 2018 datube
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this package; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
#
#    On Debian systems, the complete text of the GNU General
#    Public License can be found in `/usr/share/common-licenses/GPL-3'.


# TODO: re-encrypt existing encrypted values with new vault_pass
# TODO: ^-- walk over all files and re-encypt existing values


# import required modules  ---------------------------------------------------
import argparse         # Parser for command-line options
import base64           # RFC 3548: Base16, Base32, Base64 Data Encodings
import os               # Miscellaneous operating system interfaces
import subprocess       # Subprocess management
import sys              # System-specific parameters and functions
import tempfile         # Generate temporary files and directories


# Classes ====================================================================
class Config(): #-------------------------------------------------------------
  """Simple class to store global configuration"""
  def __init__(self):
    # default action
    self.action     = 'show_pass'
    # extensions able to be read
    self.passext    = [ 'gpg', 'b64', 'txt' ]
    # current pass file
    self.passfile   = None
    # vault pass read from "known" file
    self.cur_passwd = None
    # current passwd file extension
    self.cur_passext= None

    # fix self.pass_ext
    self.passext    = [ '.{}'.format(e) for e in self.passext ]

    # force re-encryption of pass file?
    self.force_pass_encryption = False
    # start in parent folder (if this script is located in a subfolder)
    self.start_in_parent = False

    # what's known about "this" script
    self.script     = os.path.abspath(__file__)
    self.basename   = os.path.basename(self.script)
    self.dirname    = os.path.dirname(self.script)
    (self.root,
        self.ext)   = os.path.splitext(self.basename)
    self.cwd        = os.getcwd()

    # python gnupg options
    self.gpgbinary  = '/usr/bin/gpg2'
    self.pubkeypath = '{}/.{}'.format(self.dirname, self.root)

    # did we receive any arguments?
    self.cli_arguments = False
    # suppress stdout?
    self.quiet = False
    # output colon separated
    self.with_colons = False


class GnuPG(): #--------------------------------------------------------------
  """Simple wrapper for /usr/bin/gpg2"""
  # notes:
  # - uses subprocess.Popen(shell=False)!
  # - it "raise"s simple errors and currently does
  #   not do extensive input/output/error checking
  # - defaults to a temporarily GNUPGHOME in '/tmp/'
  # - only accepts armored public gpg keys
  # - expects user to have keyring setup properly

  def __init__(self, tempgnupg=True):
    self.gnupghome = None
    self.env       = None
    # explicitly specify to use the current environment
    if tempgnupg:
      self.gnupghome  = tempfile.mkdtemp(dir='/tmp')
      self.env        = { 'GNUPGHOME': self.gnupghome }
    #
    self.gpgbinary    = '/usr/bin/gpg2'
    self.tempgnupg    = tempgnupg
    # minimal command to execute (default options)
    self.def_args     = [ self.gpgbinary, '--batch' ]

    # is gnupgbinary available?
    if not os.path.exists(self.gpgbinary):
      raise Exception("required binary `{}' not found".format(self.gpgbinary))

  def __popen__(self, args, input=None):
    args = self.def_args + args
    # start a new child process
    cp = subprocess.Popen(args,
        stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
        shell=False, env=self.env)
    out, err = cp.communicate(input)
    return out, err, cp.returncode

  def __del__(self):
    if self.tempgnupg:
      if self.gnupghome:
        if os.path.exists(self.gnupghome) and os.path.isdir(self.gnupghome):
          os.system('rm -r {}'.format(self.gnupghome))
      # there is (probably) a new gpg-agent spawned:
      # terminate it, it's useless and just uses system-resources
      args = [ 'ps', '-C', 'gpg-agent', '-ao', 'pid=,cmd=' ]
      out, err, ret = self.__popen__(args)
      for proc in out.split('\n'):
        if proc.find(self.gnupghome) > 0:
          pid = proc.strip().split(' ')[0]
          os.kill(int(pid), 15)

  #-------------------------------------------------------

  def import_key(self, pubkey):
    l = open(pubkey,'r').readlines()
    if len(l)>0:
      l = [ l for l in l if l.strip() ]
      # armored key check
      is_armored = (
          l[0].strip() == '-----BEGIN PGP PUBLIC KEY BLOCK-----' and
          l[-1].strip() == '-----END PGP PUBLIC KEY BLOCK-----' )
      if not is_armored:
        raise Exception('not an armored public key')
      else:
        args = [ '--import', pubkey ]
        out, err, ret = self.__popen__(args)
        if ret != 0:
          raise Exception(str(err).strip())

  def list_keys(self, secret=False):
    keys = []
    validity = { # purely cosmetic (see gnupg2 doc/details)
        '-':'unknown','n':'not', 'm':'marginal','f':'fully','u':'Ultimate',
        'e':'expired', 'i':'invalid', 'r':'revoked', 'd':'disabled',
        's':'self-signed', '':'GnuPG<2.1'}
    args = [ '--with-colons', '--list{}-keys'.format(
      ('-secret' if secret else '')) ]
    out, err, ret = self.__popen__(args)
    if ret != 0:
      raise Exception(str(err).strip())
    else:
      records = out.split('\n')
      idx = 0
      while idx < len(records):
        key = None
        record = records[idx].split(':')
        if record[0] in ['pub','sec']: # start public/secret key records
          key = { 'trust': validity[record[1]] }
          while 1: # loop until ..
            idx += 1
            record = records[idx].split(':')
            if record[0] == 'fpr': # fingerprint
              key['fingerprint'] = record[9]
            if record[0] == 'uid': # first uid record
              key['owner'], key['uid'] = record[9].rsplit(' ', 1)
              key['uid'] = key['uid'].replace('<','').replace('>','')
              # ignore subkeys / other uid's
              break
            if idx+1 >= len(records) or record[0] == 'pub':
              idx -= 1
              break
          keys.append(key)
        idx += 1
      return keys

  def trust_key(self, fingerprint=None, all=False):
    args = [ '--import-ownertrust' ]
    fingerprints = []
    if all:
      fingerprints = [ key['fingerprint'] for key in self.list_keys() ]
    else:
      if fingerprint:
        fingerprints = [ fingerprint ]

    if fingerprints:
      for fingerprint in fingerprints:
        input = '{}:6:\n'.format(fingerprint)
        out, err, ret = self.__popen__(args, input=input)
        if ret != 0:
          raise Exception("unable to trust fingerprint `{}'".format(
            fingerprint))

  def encrypt(self, s, uids=[], armor=False):
    if type(uids) == str:
      uids = [ uids ]
    recipients = []
    for uid in uids:
      recipients.append('-r')
      recipients.append(uid)
    args = [ '--encrypt' ] + recipients
    if armor:
      args += [ '--armor' ]
    input = '{}\n'.format(s)
    out, err, ret = self.__popen__(args, input=input)
    if ret != 0:
      raise Exception(str(err).strip())
    else:
      return out

  def decrypt(self, s, isfile=False):
    args = [ '--decrypt', ( s if isfile else '') ]
    input = ('{}\n'.format(s) if not isfile else None)
    out, err, ret = self.__popen__(args, input=input)
    if ret != 0:
      raise Exception(str(err).strip())
    else:
      return out


class Indexer(): #------------------------------------------------------------
  """Indexes ansible-vault encrypted values within file"""

  class _avev_(): # ansible vault encrypted variable
    def __init__(self):
      self.lineno = -1
      self.indent = -1
      self.name   = None
      self.value  = None

  def __init__(self, filename=None):
    self.filename = filename
    self.vault_vars = []
    self.vault_suffix = '!vault |'
    # automagically invoke __index__
    self.__index__()

  def __del__(self):
    pass

  def __index__(self): # automagically called when class created
    lines = open(self.filename,'r').readlines()
    lines = [ l.rstrip() for l in lines ]

    # find any line containing "!vault |" and index it
    for idx, line in enumerate(lines):
      if line.strip().endswith(self.vault_suffix): # found one
        line = line.rstrip(self.vault_suffix)
        var = self._avev_()
        var.lineno = idx # line number where it is found
        if line[0].isspace(): # variable is indented somehow
          var.name = line.rsplit()[-1:][0][:-1]
        else:
          var.name = line[:-1]
        # get ansible-vault encrypted value; starts at next line.
        # note: ansible requires you to indent the encrypted value with at
        # least one space. futhermore: ansible-vault hexlify's the encrypted
        # value which implies only hexadecimal allowed characters.
        # .. in ansiblecode somewhere this is
        idx += 1
        line = lines[idx]
        var.indent = len(line) - len(line.lstrip())
        if var.indent != 0: # otherwise skip it
          var.value = [line.strip()]
          idx += 1
          while idx < len(lines):
            indent = len(lines[idx]) - len(lines[idx].lstrip())
            not_hexlified = len([ c for c in lines[idx].strip() if not c in
              '0123456789abcdef']) != 0
            if indent != var.indent or not_hexlified:
              break
            var.value.append(lines[idx].strip())
            idx += 1
          var.value = '\n'.join(var.value)
          self.vault_vars.append(var)

  def count(self): # return number found vault encrypted variables
    return len(self.vault_vars)

  def find(self, s, with_colons = False): # simple search engine
    found_vars = []
    for var in self.vault_vars:
      if s == '*':
        found_vars.append(var)
      elif s.startswith('*') and s.endswith('*'):
        if var.name.find(s[1:-1]) >= 0:
          found_vars.append(var)
      elif s.startswith('*'):
        if var.name.endswith(s[1:]):
          found_vars.append(var)
      elif s.endswith('*'):
        if var.name.startswith(s[:-1]):
          found_vars.append(var)
      else:
        if var.name == s:
          found_vars.append(var)

    result = []
    msg_fmt = ('{}:{}:{}' if with_colons else '{}: "{}": at line {}')
    for var in found_vars:
      msg = msg_fmt.format(self.filename, var.name, var.lineno+1)
      result.append(msg)
    return result

  def get_var(self, lineno=0):
    ret = None
    for var in self.vault_vars:
      if var.lineno == int(lineno) -1:
        ret = var
    return ret


# Functions ==================================================================
def parse_arguments_init(): #-------------------------------------------------
  """Initialize argument parsing"""

  p = argparse.ArgumentParser(add_help=False,
      formatter_class=argparse.RawTextHelpFormatter,
      description="without any arguments reads/decrypts `{}'".format(c.root)+
      ' with one of the\nfollowing extensions: {}'.format(', '.join(
      c.passext)))

  p.add_argument('-h', dest='help',
      help='show this help message and exit',
      action='store_true', default=False)
  p.add_argument('-q', dest='quiet',
      help="try to be quiet (if applicable)".format(c.root),
      action='store_true', default=False)
  p.add_argument('-f', dest='find', metavar='search_string',
      help='find ansible-vault encrypted variable(s)',
      action='store', default=None)
  p.add_argument('-d', dest='decrypt', metavar='file:lineno',
      help='decrypt ansible-vault value',
      action='store', default=None)
  p.add_argument('--re-encrypt', dest='reencrypt',
      help="re-encrypt `{}.gpg'".format(c.root),
      action='store_true', default=False)
  p.add_argument('--update-password', dest='newmasterpass',
      metavar='<new password>',
      help='update password and re-encrypt embedded values (nyi)',
      action='store', default=None)
  p.add_argument('--with-colons', dest='with_colons',
      help='listings delimited by colons',
      action='store_true', default=False)
  p.add_argument('--force-pass-encryption', dest='force_pass_encryption',
      help="forcefully re-encrypt `{}.gpg'".format(c.root),
      action='store_true', default=False)

  return p


def parse_arguments(): #------------------------------------------------------
  """Parse command-line arguments"""

  parser = parse_arguments_init()

  args = parser.parse_args()

  if args.decrypt:
    c.cli_arguments = True
    c.action = 'decrypt'
    c.decrypt = args.decrypt
  if args.find:
    c.cli_arguments = True
    c.action = 'find'
    c.find = args.find
  if args.help:
    c.cli_arguments = True
    parser.print_help()
    sys.exit(0)
  if args.quiet:
    c.quiet = args.quiet
  if args.reencrypt:
    c.cli_arguments = True
    c.action = 'reencrypt'
  if args.force_pass_encryption:
    c.cli_arguments = True
    c.force_pass_encryption = args.force_pass_encryption
  if args.with_colons:
    c.cli_arguments = True
    c.with_colons = args.with_colons


def log_msg(msg, iostream=sys.stdout, maxlen=78): #---------------------------
  """Log message to either stdout or [iostream]"""
  if len(msg)>maxlen:
    msg = msg[:3]+(msg[maxlen+6:] and '...') + msg[-maxlen+6:]
  if not msg.endswith('\n'):
    msg += '\n'
  # we do not detect if called from ansible; if so writing a message to
  # stdout would result in errors; that's why (passed) iostream is used
  iostream.write(msg)
  iostream.flush()


def gpg_reencrypt_passfile(): #-----------------------------------------------
  """Re-encrypt .gpg passfile"""
  passwd = None

  # retrieve the current password (using user's current gpg keyring)
  passwd = get_pass()
  if not passwd:
    # something went wrong; the user is already notified
    sys.exit(1)
  if not c.passfile.endswith('.gpg'):
    log_msg('E: no gpg pass file to re-encrypt', sys.stderr)
    sys.exit(1)

  pubkey_files = []
  if os.path.exists(c.pubkeypath):
    for f in os.listdir(c.pubkeypath):
      f = os.path.join(c.pubkeypath, f)
      if os.path.isfile(f) and f.endswith('.asc'):
        pubkey_files.append(os.path.join(c.dirname, f))

  if len(pubkey_files) == 0:
    log_msg('E: no public gpg key(s) found (.asc)', sys.stderr)
    sys.exit(1)
  else:
    if not c.quiet:
      log_msg('I: found {} public gpg key(s)'.format(len(pubkey_files)))

  # get fingerprints user's private keys (does not read actual key!)
  user_keys = []
  if not c.force_pass_encryption:
    gpg = GnuPG(tempgnupg=False)
    user_keys = gpg.list_keys(secret=True)
    gpg = None

  # initialize simple gnug2 wrapper class (using temp gnupg home)
  gpg = GnuPG()

  # import (armored) public keys
  for pubkey in pubkey_files:
    try:
      gpg.import_key(pubkey)
    except Exception, e:
      pubkey = pubkey.rsplit('/',1)[1]
      log_msg("E: unable to import key `{}'".format(pubkey), sys.stderr)
      for l in str(e).split('\n'):
        log_msg("E> {}".format(l), sys.stderr, 999)
      sys.exit(1)

  # trust all newly added keys
  try:
    gpg.trust_key(all=True)
  except Exception, e:
    log_msg('E: {}'.format(e), sys.stderr, 999)
    sys.exit(1)

  # now we have a list of keys we need to re-encrypt, but does it also contains
  # the user's caller key(s)? if not, re-encryption probably isn't a good idea,
  # unless the script is called with "--force-pass-encryption"
  fingerprints       = [ k['fingerprint'] for k in gpg.list_keys() ]
  user_fingerprints  = [ k['fingerprint'] for k in user_keys ]
  found_fingerprints = [ f for f in fingerprints if f in user_fingerprints ]
  if len(found_fingerprints) == 0:
    if c.force_pass_encryption:
      log_msg("W: missing user's caller key!", sys.stderr)
    else:
      log_msg("E: missing user's caller key; refuse to continue", sys.stderr)
      sys.exit(1)

  if not c.quiet:
    log_msg("R: re-encrypt `{}'".format(os.path.basename(c.passfile)))
  # prepare re-encryption
  passfile_backup = '{}.bak'.format(c.passfile)
  if os.path.exists(passfile_backup):
    os.remove(passfile_backup)
  os.rename(c.passfile, passfile_backup)

  # everything in place; do the actual re-encryption ------
  encrypted_value = None
  uids = []
  for key in gpg.list_keys():
    uids.append(key['uid'])
  try:
    encrypted_value = gpg.encrypt(passwd, uids)
  except:
    log_msg("E: unable to re-encrypt pass", sys.stderr)
    sys.exit(1)

  # passwd is re-encrypted; write it out
  try:
    open(c.passfile,'w').write(encrypted_value)
  except:
    log_msg("E: could not write to `{}'".format(
      os.path.basename(c.passfile)), sys.stderr)

  # and done
  gpg = None

  if not c.quiet:
    log_msg('I: finished')


def ansible_find_variable(): #------------------------------------------------
  """Find ansible-vault encrypted variabele(s)"""

  for root, dirs, files in os.walk(c.cwd):
    for filename in files:
      if filename.endswith('.yml') or filename == 'all':
        fn = os.path.relpath(root, c.cwd)
        fn = os.path.join(fn, filename)
        if not os.path.islink(fn):
          try:
            indexer = Indexer(fn)
          except Exception, e:
            for l in str(e).split('\n'):
              log_msg("E: {}".format(l), sys.stderr, 999)
            sys.exit(1)

          results = indexer.find(c.find, with_colons=(
            True if c.with_colons else False))
          for result in results:
            log_msg('{}{}'.format(('' if c.with_colons else 'F: '), result),
                maxlen=999)


def ansible_decrypt_value(): #------------------------------------------------
  """Decrypt an ansible-vault encrypted value"""

  passwd = get_pass()
  if not passwd:
    # something went wrong; the user is already notified
    sys.exit(1)

  fn, lineno = c.decrypt.split(':')

  if not lineno.isdigit():
    log_msg('E: incorrect line number "{}"'.format(lineno), sys.stderr)
    sys.exit(1)

  # try see if there is a variable at given lineno
  try:
    indexer = Indexer(fn)
  except Exception, e:
    for l in str(e).split('\n'):
      log_msg("E: {}".format(l), sys.stderr, 999)
    sys.exit(1)
  var = indexer.get_var(lineno)
  if not var:
    log_msg("E: no variable found at line {}".format(lineno), sys.stderr)
    sys.exit(1)
  if not c.quiet:
    log_msg("I: search in `{}'".format(fn))

  # requested file is okay; onto next part
  # setup ansible vault and try decryption
  import ansible.parsing.vault as apv
  passwd = apv.VaultSecret(_bytes=apv.to_bytes(passwd))
  ansible_vault = apv.VaultLib(secrets=[(u'default',passwd)])
  if not apv.is_encrypted(var.value):
    log_msg("E: not an ansible-vault encrypted value", sys.stderr)
    sys.exit(1)

  # all looks good to go
  decrypted_value = None
  if not c.quiet:
    log_msg("D: `{}' at line {}".format(var.name, lineno))
  try:
    decrypted_value = ansible_vault.decrypt(var.value)
  except Exception, e:
    for l in str(e).split('\n'):
      log_msg("E: {}".format(l), sys.stderr, 999)
  if decrypted_value:
    print(decrypted_value)


def get_pass(): #-------------------------------------------------------------
  """Return available password"""
  # detect if there is a vault_pass.* file; if there is,
  # try to read/decode/decrypt it and return the plain content
  passwd  = None
  basefunc= 'handle_get_pass_'

  # try *.ext in order; first is more secure than the latter
  # if multiple pass files found it will refuse to provide
  # passwd and show error.
  passfilecount = 0
  for e in c.passext:
    fn = '{}{}'.format(c.root, e)
    fn = os.path.join(c.dirname, fn)
    if os.path.exists(fn):
      c.passfile = fn
      passfilecount+=1

  if passfilecount == 1:
    f,e = os.path.splitext(c.passfile)
    func = '{}{}'.format(basefunc, e[1:])
    passwd = globals()[func](c.passfile)
    if passwd:
      passwd = passwd[0].strip()
      c.cur_passwd = passwd
    else:
      passwd      = None
      c.passfile  = None
  elif passfilecount > 1:
    log_msg('E: multiple pass file detected; refuse to continue',
        sys.stderr)
    sys.exit(1)
  elif passfilecount == 0:
    log_msg('E: no pass file available (*{})\n'.format(
        ', *'.join(c.passext)), sys.stderr)
    sys.exit(1)

  return passwd


# Handlers: get password from file ===========================================
def handle_get_pass_txt(f): #-------------------------------------------------
  """Handles .txt passfile"""
  passwd = None
  fn = os.path.join(c.dirname, f)
  if os.path.exists(fn):
    if not c.quiet:
      log_msg('I: {}'.format(fn), sys.stderr)
    passwd  = open(fn, 'r').readlines()
  if len(passwd) == 0:
    passwd = None

  return passwd


def handle_get_pass_b64(f): #-------------------------------------------------
  """Handles .b64 passfile"""
  passwd = None
  fn = os.path.join(c.dirname, f)
  if os.path.exists(fn):
    if not c.quiet:
      log_msg('I: {}'.format(fn), sys.stderr)
    passwd = open(fn, 'r').readlines()
  if passwd:
    try:
      passwd = ''.join(passwd)
      passwd = base64.decodestring(passwd)
      passwd = passwd.split('\n')
    except:
      log_msg('E: incorrect base64 encoded string', sys.stderr)
      passwd = None

  return passwd


def handle_get_pass_gpg(f): #-------------------------------------------------
  """Handles .gpg passfile"""
  passwd = None

  # set up gnupg (we expect the user to have a proper key setup)
  gpg = GnuPG(tempgnupg=False)
  fn = os.path.join(c.dirname, f)
  if os.path.exists(fn):
    if not c.quiet:
      log_msg('I: {}'.format(fn), sys.stderr)
    try:
      passwd = gpg.decrypt(c.passfile, isfile=True)
    except Exception, e:
      if not c.quiet:
        for l in str(e).split('\n'):
          log_msg("E: {}".format(l), sys.stderr, 999)
      log_msg("E: unable to decrypt `{}'".format(os.path.basename(c.passfile)),
          sys.stderr)

  if passwd:
    if len(passwd) == 0:
      passwd = None
    else:
      passwd = passwd.split('\n')

  return passwd


# ============================================================================
# ============================================================================
# ============================================================================
if __name__ == '__main__':

  # main configuration
  c = Config()

  # handle given arguments
  parse_arguments()

  # decide what to do
  if c.action == 'show_pass' and not c.cli_arguments:
    passwd = get_pass()
    if passwd:
      # ansible(-vault) doesn't care if the password contains a newline or not
      print(passwd)
    else:
      log_msg('E: no password available (*{})\n'.format(
        ', *'.join(c.passext)), sys.stderr)
  elif c.action == 'reencrypt':
    gpg_reencrypt_passfile()
  elif c.action == 'find':
    ansible_find_variable()
  elif c.action == "decrypt":
    ansible_decrypt_value()
  else:
    log_msg("E: don't know what do do?", sys.stderr)
    sys.exit(1)
