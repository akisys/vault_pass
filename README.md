# vault_pass.py

 * [introduction](./README.md#introduction)
 * [requirements](./README.md#requirements)
 * [usage](./README.md#usage)
 * [initialize vault password](./README.md#initialize-vault-password)
   * [gpg encrypted vault password](./README.md#gpg-encrypted-vault-password)
 * [create ansible-vault encrypted variable/value pair](./README.md#create-ansible-vault-encrypted-variablevalue-pair)
 * [find ansible-vault encrypted variable](./README.md#find-ansible-vault-encrypted-variable)
 * [decrypt ansible-vault encrypted variable](./README.md#decrypt-ansible-vault-encrypted-variable)
 * [update ansible-vault encrypted variable](./README.md#update-ansible-vault-encrypted-variable)
 * [change vault password](./README#change-vault-password)


## introduction

Using the [ansible-vault](https://docs.ansible.com/ansible/2.4/vault.html)
, with a "vault password" you are able to encrypt a:
 1. *whole file* containing variables/values
 2. *single* value

When you are using:
 1. you cannot search (grep) for variables
 2. it's currently (20180928) really hard to re-encrypt / decrypt all values

To overcome these "challenges", you can use `vault_pass.py`: it allows you to
encrypt a *single* value and use it in your playbooks and provides the "vault
password" to "ansible". If you decide to change the "vault password", this
script will take care of re-encrypting *all* already supplied *single*
encrypted variable values!

*But there is more...*

When working on a project with several (maybe changing) team members,
"ansible-vault" does not provide you with methods to keep the "vault password"
really secret. Using gpg you are able to (re)encrypt the "vault password" to be
only viewable for one of the "recipients", but you are required to maintain
keys (sign/delete) and manually re-encrypt/decrypt the "vault password".

To ease the maintenance of signing/deleting keys for any of the "recipients"
and to re-encrypt the "vault password", `vault_pass.py` will provide you with
an easy method to take care of that.

Furthermore, `vault_pass.py` helps you to:
  1. find encrypted variables and retrieve the actual value
  1. update an already encrypted value


Native ansible-vault encryption/decryption methods and the well established
`gpg2` binary are used to accomplish the tasks.

Before you continue, please read the whole document to know what to expect.

*Note: vault_pass.py is in no way affiliated with, endorsed and/or sponsored by
Ansible<sup>(R)</sup>*


## requirements

```bash
apt-get install gnupg2
```


## usage

Copy `vault_pass.py` to a location somewhere in the root of your ansible
project as you see fit (also see *initialize vault password*).

```bash
# ansible.cfg example (adjust as required)
[defaults]
vault_password_file = scripts/vault_pass.py
```

```bash
./scripts/vault_pass.py -h
```

*Note: make sure you mark `vault_pass.py` executable, otherwise ansible will
use the python code for the password.*

## initialize vault password

Copy `vault_pass.py` to a location somewhere in the root of your ansible
project as you see fit, just ensure that `vault_pass.py` and one of the
following "vault password" files are at the same location.

```bash
cd ./scripts
# very unsecure password
pwgen -sy 32 | shuf > vault_pass.txt
# less very unsecure password
pwgen -sy 32 | shuf | base64 -w0 > vault_pass.b64
# secure password (see "gpg encrypted vault pass")
pwgen -sy 32 | shuf | gpg2 -e --default-recipient-self > vault_pass.gpg
```
Note: do not execute any of the above commands if you already executed *create
vault encrypted variable/value pair*: you will lose access to those!


### gpg encrypted vault password

When you have executed *initialize vault password* (using gpg), it's (probably)
time to add other trustworthy users to be able to retrieve the "vault
password".  
Use the following layout:

```bash
.
├── vault_pass.py
├── vault_pass.gpg
└──.vault_pass/
   ├── your@public.key.asc
   ├── some1@trusted.usr.asc
   └── some2@trusted.usr.asc
```

Add any trusted (armored) exported public keys to the ".vault_pass" folder and
do not forget to add you're own (armored) public key.

```bash
./scripts/vault_pass.py --re-encrypt
I: /home/user/ansible/vault_pass.gpg
I: found 3 public gpg key(s)
R: re-encrypt `vault_pass.gpg'
I: finished
```

If you forgot to include you're own public key, you will receive the following
message:

```bash
E: missing user's caller key; refuse to continue
```

Repeat the process when keys are added, updated and/or removed. That allows you
to safely store the "vault password" and the public keys in a (remote)
repository.


## create ansible-vault encrypted variable/value pair

When you have correctly setup the *initialize vault password*, it's time to
[encrypt variables to embed in yaml](https://docs.ansible.com/ansible/2.4/vault.html#use-encrypt-string-to-create-encrypted-variables-to-embed-in-yaml).

```bash
echo "secretvalue" | ansible-vault encrypt_string --stdin-name 'variable'
```

..and copy the output into the proper file for usage within your ansible
playbook(s).


## find ansible-vault encrypted variable

When you have any *ansible vault encrypted variable*(s), at some point in time
you probably want to get access to an encrypted  value (for ex. to do some
testing). There is a basic "search-engine" provided to find them:

```bash
./scripts/vault_pass.py -f "variable" # returns exact matches
F: ansible/defaults/main.yml: "variable": at line 12
```

You can use an asterisk ("\*") to enhance your search:
  1. "\*var": variable name ends with "var"
  1. "var\*": variable name starts with "var"
  1. "\*var\*": variable name contains "var"
  1. "\*": matches all variables


## decrypt ansible-vault encrypted variable

If you have found any variable for which you would like to know the secret
value, you can decrypt as follows:

```bash
./scripts/vault_pass.py -qd ansible/defaults/main.yml:12
decrypted secret value
```

## update ansible-vault encrypted variable

You can easily set a new value for a already encrypted variable, first "find
it" and then issue the following command to update to a new value:

```bash
echo 'secret value' | ./scripts/vault_pass.py -qu ansible/defaults/main.yml:12
# or
./scripts/vault_pass.py -u ansible/defaults/main.yml:12
Reading plaintext input from stdin. (ctrl-d to end input)
```


## change vault password

When you want to change the "vault password", you must use `vault_pass.py` to
ensure all already ansible-vault encrypted values will be decryptable with the
new password. A backup will be created for every file which contains a
ansible-vault encrypted variable. Also the supplied "vault password" will be
stored in the same format already provided. Do note that the same rules apply
as with re-encrypting the "vault password".

```bash
pwgen -sy 32 | shuf | ./scripts/vault_pass.py --update-password
```

It will keep you informed during the whole process (even -q doesn't supresses
the messages). Keep your fingers crossed and hope that all goes well.
If you accidentally hit "ctrl-c" during this process, no worries, `vault_pass.py`
will do it's best to revert all changes made.

Of course do a check afterwards:

```bash
./scripts/vault_pass.py -qd ansible/defaults/main.yml:12
```
